package main

import (
	"encoding/json"
	"fmt"
)

func mapcopy(s map[string]any) (d map[string]any) {
	d = make(map[string]any, len(s))
	for k, v := range s {
		switch v_ := v.(type) {
		case map[string]any:
			d[k] = mapcopy(v_)
		case []any:
			d[k] = slccopy(v_)
		default:
			d[k] = v_
		}
	}
	return
}
func slccopy(s []any) (d []any) {
	d = make([]any, len(s))
	for k, v := range s {
		switch v_ := v.(type) {
		case map[string]any:
			d[k] = mapcopy(v_)
		case []any:
			d[k] = slccopy(v_)
		default:
			d[k] = v_
		}
	}
	return
}
func main() {
	fmt.Println("Hello World!")

	type (
		M map[string]any
		S []any
	)
	a := S{
		M{
			"a": 1,
			"b": "b",
			"c": M{
				"c": 2,
				"d": "d",
				"e": S{
					3,
					"f",
					M{
						"g": 4,
					},
				},
			},
		},
		S{1, "a", M{"b": "b"}},
		M{"a": S{1, "b"}},
	}
	c := slccopy(a)
	fmt.Println(c)
	bs, err := json.MarshalIndent(&c, "", "  ") // check is valid json
	if err != nil {
		println(err.Error())
		return
	}
	println(string(bs))
	b := M{
		"a": 1,
		"b": S{
			"c",
			M{
				"d": "d",
				"e": S{
					"f",
					M{"g": "g"},
				},
			},
		},
		"h": S{1, S{"i"}},
		"j": M{"k": "l"},
	}
	d := mapcopy(b)
	fmt.Println(d)
	bs, err = json.MarshalIndent(&d, "", "  ") // check is valid json
	if err != nil {
		println(err.Error())
		return
	}
	println(string(bs))
}
